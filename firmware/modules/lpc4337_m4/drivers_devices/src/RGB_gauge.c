/*
 * RGB_gauge.c
 *
 *  Created on: 2 nov. 2021
 *      Author: juani
 */

#include "systemclock.h"
#include "delay.h"
#include "NeoPixel.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
pixel vector_RGB[12] = {};
uint16_t g_min=0;
uint16_t g_max=0;
uint16_t l_pixel=0;
/*==================[internal functions declaration]=========================*/
void InitRGB_Gauge(uint8_t n_pixel, uint8_t gpio_pin, uint16_t min, uint16_t max){
	NeoPixelInit(gpio_pin, n_pixel);
	g_min = min;
	g_max = max;
	l_pixel = n_pixel;
}

static uint32_t Color(uint8_t r, uint8_t g, uint8_t b) {
    return ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
  }

uint32_t wheel(uint8_t WheelPos) {
if(WheelPos < 85) {
	return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
else if(WheelPos < 170) {
	WheelPos -= 85;
return Color(255 - WheelPos * 3, 0, WheelPos * 3);
}
else {
	WheelPos -= 170;
return Color(0, WheelPos * 3, 255 - WheelPos * 3);
}
}

void clearStrip(void){
	int i = 0;
	for(i = 0; i < l_pixel; i++) {
		vector_RGB[i].R = 0;
		vector_RGB[i].G = 0;
		vector_RGB[i].B = 0;
	}
}

long map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void Gauge_line (uint16_t pres) {
	uint8_t t= map(pres, g_min, g_max, 0, l_pixel);
	clearStrip();
	uint16_t L;
	uint8_t WheelPos;
	uint8_t offset = 25;
	for(uint16_t L = 0; L<t; L++) {
		WheelPos=wheel(((170+(L*3)) & 255));
		if(WheelPos < 85) {
			//return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
			vector_RGB[L].R = WheelPos * 4 - offset;
			vector_RGB[L].G = 255 - WheelPos * 3 - offset;
			vector_RGB[L].B = 0;
			//NeoPixelSingleLed(WheelPos * 3, 255 - WheelPos * 3, 0,L,0);
		}
		else if(WheelPos < 170) {
			WheelPos -= 85;
			vector_RGB[L].R = 255 - WheelPos * 3 - offset;
			vector_RGB[L].G = 0;
			vector_RGB[L].B = WheelPos * 3 - offset;
		}
		else {
			WheelPos -= 170;
			vector_RGB[L].R = 0;
			vector_RGB[L].G = WheelPos * 3 - offset;
			vector_RGB[L].B = 255 - WheelPos * 3 - offset;
		}
	}
	DelayUs(50);
	NeoPixelVector(vector_RGB);
	DelayMs(10);
}

