﻿/* Copyright 2019,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Bare Metal driver for switchs in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  SM			Sebastian Mateos
 *  EF			Eduardo Filomena
 *  JMR			Juan Manuel Reta
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version leo
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "../inc/XFPM_050KPGR.h"      /* <= own header */
#include "gpio.h"
#include "analog_io.h"
#include "chip.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
float alfa = 0.018;
float beta = 0.04;
uint8_t Vs = 5;
float Presion;
uint16_t Vout;
analog_input_config SensorAD;

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
uint8_t SensorInit(uint8_t entrada_sensor)
{	//es un pasamanos con analog.io
	//Modifico el nombre de Struct

	SensorAD.input = entrada_sensor;
	SensorAD.mode = AINPUTS_SINGLE_READ;//este lo dejo fijo x eso no paso x la funcion
	SensorAD.pAnalogInput = NULL;//este lo dejo fijo x eso no paso x la funcion

	AnalogInputInit(&SensorAD);//AnalogInputInit()es de analog_io
	return (true);
}
/*=============*/
uint16_t Leo_Dato_Crudo(void)
{
	uint16_t valor;
	AnalogInputReadPolling(SensorAD.input, &valor);//SensorAD.input es el chanel con q configuraste
												   // el sensor en SensorInit()

	return valor;
}
/*=============*/
float Leo_Presion(void)
{
	Vout = Leo_Dato_Crudo();

	Presion = ((Vout/Vs)-beta)/(10 * alfa);
	return(Presion);
}
/*=============*/


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
