/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup roll_plot Roll Plot
 ** @{ 
 *
 * @brief Driver for drawing Roll Plots on the TFT LCD RGB 240x320 ILI9341 
 *
 * @author Albano Peñalva
 *
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_REALTIMEPLOT_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_REALTIMEPLOT_H_

#include <stdint.h>

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/**
 * @brief
 */
typedef struct
{
	uint16_t x_pos;		/*!< x position of top left corner of plot */
	uint16_t y_pos;		/*!< y position of top left corner of plot */
	uint16_t width;		/*!< plot width */
    uint16_t height; 	/*!< plot height */
    uint16_t x_scale;	/*!< x scale in % (number of pixels drawn per 100 data samples) */
    uint16_t back_color;/*!< plot background color */
} plot_t;

/**
 * @brief
 */
typedef struct
{
	uint16_t y_scale;	/*!< y scale in % (number of pixels drawn per 100 data value) */
    uint16_t y_offset; 	/*!< y offset */
	uint16_t color;		/*!< plot color */
	uint16_t x_prev;	/*!< x position of last point drawn */
	uint16_t y_prev;	/*!< y position of last point drawn */
	plot_t * plot;		/*!< plot in which the signal'll be drawn */
} signal_t;

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief  		Initializes a plot
 * @param[in]  	plot: Structure with the plot configuration
 * @retval 		NONE
 */
void RollPlot_Init(plot_t * plot);

/**
 * @brief  		Initializes parameters of a signal for a specific plot
 * @param[in]  	plot: Structure with the plot configuration
 * @param[in]  	signal: Structure with the signal configuration
 * @retval 		NONE
 */
void RollPlot_SignalInit(plot_t * plot, signal_t * signal);

/**
 * @brief		Write data to SPI port
 * @param[in]	plot: Structure with the plot configuration
 * @param[in]  	signal: Structure with the signal configuration
 * @param[in]	data: Data value to draw in plot
 * @return  	None
 * @note
 */
void RollPlot_Draw(signal_t * signal, uint16_t data);

 /** @} doxygen end group definition */
 /** @} doxygen end group definition */
 /** @} doxygen end group definition */
 
#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_REALTIMEPLOT_H_ */
