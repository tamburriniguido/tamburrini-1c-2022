/*
 * RGB_gauge.h
 *
 *  Created on: 2 nov. 2021
 *      Author: juani
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_RGB_GAUGE_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_RGB_GAUGE_H_
/**
 * @brief Init RGB module to be used as analog gauge
 * @param[in] n_pixel number of pixels
 * @param[in] gpio_pin gpio pin used
 * @param[in] min minimum value to map in gauge
 * @param[in] max maximum value to map in gauge
 * @return None
 */
void InitRGB_Gauge(uint8_t n_pixel, uint8_t gpio_pin, uint16_t min, uint16_t max);

/**
 * @brief Send value to show in gauge
 * @param[in] value Value to show
 * @return None
 */
void Gauge_line(uint16_t value);



#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_RGB_GAUGE_H_ */
