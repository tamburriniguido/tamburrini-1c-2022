/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/

/*Escribir una función que reciba como parámetro
 * un dígito BCD y  un vector de estructuras del
 * tipo  gpioConf_t.

typedef struct
{
	uint8_t port;				!< GPIO port number
	uint8_t pin;				!< GPIO pin number
	uint8_t dir;				!< GPIO direction ‘0’ IN;  ‘1’ OUT
} gpioConf_t;

Defina un vector que mapee los bits de la siguiente
manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14

La función deberá establecer en qué valor colocar
cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.
 */
/*==============================[NOTA]========================================*/

    // NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}

    // NOMBRE DEL #DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI

    // NOMBRE DE LAS VARIABLES-------- todo_minuscula

/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/
#define IN 0
#define OUT 1

#define MASK_1 0b00000001
#define	MASK_2 0b00000010
#define MASK_3 0b00000100
#define MASK_4 0b00001000

/*==================[Internal functions declaration]=========================*/

typedef struct
{
	uint8_t pin;				//!< GPIO pin number
	uint8_t dir;				//!< GPIO direction ‘0’ IN;  ‘1’ OUT
}gpioConf_t;

void ControlPuertos(uint8_t bcd, gpioConf_t *puerto_ptr)
{
	if(bcd && MASK_1)
		{GPIOOn(puerto_ptr[0].pin);}
		else {GPIOOff(puerto_ptr[0].pin);}

	if(bcd & MASK_2)
		GPIOOn(puerto_ptr[1].pin);
		else GPIOOff(puerto_ptr[1].pin);

	if(bcd & MASK_3)
		GPIOOn(puerto_ptr[2].pin);
		else GPIOOff(puerto_ptr[2].pin);

	if(bcd & MASK_4)
		GPIOOn(puerto_ptr[3].pin);
		else GPIOOff(puerto_ptr[3].pin);

}

int main(void)
{
	uint8_t bcd;

	//puertos es un vectos con 4 elementos.
   	gpioConf_t puertos[4],*puerto_ptr;

	puertos[0].pin = GPIO_LCD_1;
	puertos[1].pin = GPIO_LCD_2;
	puertos[2].pin = GPIO_LCD_3;
	puertos[3].pin = GPIO_LCD_4;

	bcd = 3;
	while(1)
		{
		ControlPuertos(bcd, puerto_ptr);
		sleep(10);
		}

	return 0;
}

/*==================[End of file]============================================*/


