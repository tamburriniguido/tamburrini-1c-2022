/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */
/*
1)
Conectar a la EDU-CIAA un sensor de
ultrasonido HC-SR04 y una pantalla LCD
y utilizando los drivers provistos por
la cátedra implementar una aplicación que
permita:

Mostrar distancia medida utilizando los
leds de la siguiente manera:

Si la distancia está entre 0 y 10 cm,
encender el LED_RGB_B (Azul).
Si la distancia está entre 10 y 20 cm,
encender el LED_RGB_B (Azul) y LED_1.
Si la distancia está entre 20 y 30 cm,
encender el LED_RGB_B (Azul), LED_1 y LED_2
Si la distancia es mayor a 30 cm, encender el
LED_RGB_B (Azul), LED_1, LED_2 y LED_3.

Mostrar el valor de distancia en cm utilizando
el display LCD.
Usar TEC1 para activar y detener la medición.
Usar TEC2 para mantener el resultado (“HOLD”).
Refresco de medición: 1 s

2)Modifique la actividad del punto 1 de manera de
utilizar interrupciones para el control de las
teclas y el control de tiempos (Timers).


ACTIVIDAD 2
Modifique la actividad del punto 1 de manera de
utilizar interrupciones para el control de las
teclas y el control de tiempos (Timers).

*/

/*==================[inclusions]=============================================*/
#include "../inc/medidor_distancia_INTERRUPCIONES_TIMER_PUERTO-SERIE.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr04.h"
#include "delay.h"
#include "gpio.h"
#include "lcditse0803.h"
#include "switch.h" // libreria para interrupciones

#include "timer.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/
uint8_t hold = 0;
uint8_t prender = 0 ;
uint8_t teclas;
uint16_t distancia = 0;

/*==================[external functions definition]==========================*/
void Led_de_Distancia (int16_t valor_distancia)
{
	if (valor_distancia < 10)
	{
		LedOn(LED_RGB_B);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if ((11 < valor_distancia) && (valor_distancia < 20))
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if ((21 < valor_distancia) && (valor_distancia < 30))
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_3);
	}
	if(valor_distancia > 30)
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
	}
}//end LedDistancia

void TeclaUno()
	{
	prender =! prender;
	DelayMs(10);
	}

void TeclaDos()
	{
	hold =! hold;
	DelayMs(10);
	}

void SysInit(void)
{
	//Inicializacion
	SystemClockInit();//inicial el clock de la CIAA
	LedsInit();
	SwitchesInit();
	LcdItsE0803Init();

	//Configuro los pines del Ultrasonido
	HcSr04Init(GPIO_T_FIL2 , GPIO_T_FIL3);

	//-----------------------------------------------
	//COSAS DE UART (serial - port)
	serial_config dato_uart;//tipo de dato del drive uart.h
	dato_uart.port = SERIAL_PORT_PC; //puerto sacado del .h
	dato_uart.baud_rate = 9600 ; //valor de velocidad,
								//debe ser el mismo que
								//la velocidad del terminal Hterm

	//dato_uart.pSerial = ;//creo q es para ingresar datos por terminal

	UartInit(&dato_uart);//paso la estructura
	//-----------------------------------------------

	//Interrupciones
	SwitchActivInt(SWITCH_1, &TeclaUno);
	SwitchActivInt(SWITCH_2, &TeclaDos);
}

void F_Distancia(void)
{
    //teclas = SwitchesRead(); NOOO VA ESTO XQ TIENE INTERRUPCIONES
    if(prender)
    {
    	distancia = HcSr04ReadDistanceInCentimeters();
    	if (!hold)
    	{
    		Led_de_Distancia(distancia);

    		//muestra los valores por LCD
    		LcdItsE0803Write(distancia);

    		//mandar los valores SERIAL-PORT
    		UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10));//10 es la base
    		UartSendString(SERIAL_PORT_PC, " \r\n");// sacado del .h
    	}
    }//fin primer if
    	else
    	{
    		LedOff(LED_RGB_R);
    		LedOff(LED_1);
    		LedOff(LED_2);
   			LedOff(LED_3);

   			//muestra los valores por LCD
   			LcdItsE0803Write(0);
   		}//fin else
}//fin F_Distancia()

int main(void){
	SysInit();
////////////////////////
	//Cosas de timer
	timer_config dato_timer;//tipo de dato del drive timer.h
	//lleno el estruct
	dato_timer.timer= 1;
	dato_timer.period = 1000;
	dato_timer.pFunc = F_Distancia;

	TimerInit(&dato_timer);
	TimerStart(dato_timer.timer);

	while(1)
    {}
	return 0;
}

/*==================[end of file]============================================*/
