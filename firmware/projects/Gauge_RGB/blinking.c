/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking.h"       /* <= own header */
#include "systemclock.h"
#include "delay.h"
#include "RGB_gauge.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "tone.h"

#define GPIO8 8
#define NUM_PIXEL 12
#define AD_SAMPLE_FREC (1000/200)
/*==================[macros and definitions]=================================*/



void StartConvertion () {
	AnalogStartConvertion();
}

void ReadData(){
	uint16_t buf;
	static uint16_t saved_data;
	AnalogInputRead(CH1, &buf);
	//clearStrip();
	Gauge_line(buf);
	UartSendString(SERIAL_PORT_PC, UartItoa(buf, 10));
	UartSendString(SERIAL_PORT_PC, (uint8_t *) "\r");

}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	InitRGB_Gauge(12,8,120,500);
	//NeoPixelInit(GPIO8, Anillo_Max2);
    uint8_t j=0;
    ToneInit();
    analog_input_config AD_config;
    AD_config.input = CH1;
    AD_config.mode = AINPUTS_SINGLE_READ;
    AD_config.pAnalogInput = ReadData;
    serial_config UART_USB;
    UART_USB.baud_rate = 115200;
    UART_USB.port = SERIAL_PORT_PC;
    UART_USB.pSerial = NULL;
    timer_config timer_config_A;
    timer_config_A.timer = TIMER_A;
    timer_config_A.period = AD_SAMPLE_FREC;
    timer_config_A.pFunc = StartConvertion;



    AnalogInputInit(&AD_config);
    UartInit(&UART_USB);


      TimerInit(&timer_config_A);
      TimerStart(TIMER_A);
    while(1)
    {


	}
    
	return 0;
}

/*==================[end of file]============================================*/

