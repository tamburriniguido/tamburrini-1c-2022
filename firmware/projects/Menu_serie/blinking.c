/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "delay.h"
#include "uart.h"
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/



rtc_t hora_config;
typedef enum  {hora, minutos, dia, mes, anio}tiempo_t;

bool set_var_1 = FALSE;
uint8_t variable_1 = 0;
bool set_var_2 = FALSE;
uint8_t variable_2 = 0;
bool set_var_3 = FALSE;
uint8_t variable_3 = 0;
bool set_clock = FALSE;

tiempo_t time_var = hora;


/*==================[internal functions declaration]=========================*/




void Menu(void){
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);
	/******* RECUPERO VALORES NUMERICOS **********/
	if(set_var_1){
		variable_1 = dat;
		/****** Para la variable #1 se verifica el valor dentro de un rango ********/
		if((variable_1<50) && (variable_1>15)){
			UartSendString(SERIAL_PORT_PC, "Variable #1:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			dat = 0;
			set_var_1 = FALSE;
			LedOff(LED_1);
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato inválido...ingrese un valor entre 10 y 50\r\n");
			dat = 0;
		}
	}
	if(set_var_2){
		variable_2 = dat;
		UartSendString(SERIAL_PORT_PC, "Variable #2:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		dat = 0;
		set_var_2 = FALSE;
		LedOff(LED_2);
	}
	if(set_var_3){
		variable_3 = dat;
		UartSendString(SERIAL_PORT_PC, "Variable #3:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		dat = 0;
		set_var_3 = FALSE;
		LedOff(LED_3);
	}
	if(set_clock){
		switch(time_var){
		case hora:
			hora_config.hour = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r\n");
			time_var = minutos;
			break;
		case minutos:
			hora_config.min = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el día:  \r\n");
			time_var = dia;
			break;
		case dia:
			hora_config.mday = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes:  \r\n");
			time_var = mes;
			break;
		case mes:
			hora_config.month = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el año:  \r\n");
			time_var = anio;
			break;
		case anio:
			hora_config.year = dat;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			time_var = hora;
			dat = 0;
			set_clock = FALSE;
			LedOff(LED_RGB_B);
			break;
		}

	}

	/******* RECUPERO COMANDOS **********/
	switch(dat){
	case 'a':
		UartSendString(SERIAL_PORT_PC, "Ingrese la variable #1:\r\n");
		set_var_1= TRUE;
		LedOn(LED_1);
		break;
	case 'b':
		UartSendString(SERIAL_PORT_PC, "Ingrese la variable #2:\r\n");
		set_var_2= TRUE;
		LedOn(LED_2);
		break;
	case 'c':
		UartSendString(SERIAL_PORT_PC, "Ingrese la variable #3:\r\n");
		set_var_3= TRUE;
		LedOn(LED_3);
		break;
	case 'd':
		UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual:\r\n");
		set_clock= TRUE;
		LedOn(LED_RGB_B);
		break;
	case 'e':
		UartSendString(SERIAL_PORT_PC, "Variable #1: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(variable_1, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
			break;
	case 'm':
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'm' para ver el menu:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'a' para configurar la variable #1:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'b' para configurar la variable #2:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'c' para configurar la variable #3:\r\n");
		/** etc...*/
		UartSendString(SERIAL_PORT_PC, "Ingrese 'e' para recuperar la variable #1:\r\n");
		/** etc...*/
		UartSendString(SERIAL_PORT_PC, "Ingrese 'd' para configurar la hora:\r\n");
		break;
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();

	serial_config UART_USB;
	UART_USB.baud_rate = 115200;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = Menu;
	UartInit(&UART_USB);


    while(1)
    {

	}
    
	return 0;
}

/*==================[end of file]============================================*/

