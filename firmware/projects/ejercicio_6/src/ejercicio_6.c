/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*
Escriba una función que reciba un dato de 32 bits,
la cantidad de dígitos de salida y dos vectores
de estructuras del tipo  gpioConf_t.
Uno  de estos vectores es igual al definido
en el punto anterior y el otro vector mapea
los puertos con el dígito del LCD a donde
mostrar un dato:

Dígito 1 -> GPIO1
Dígito 2 -> GPIO3
Dígito 3 -> GPIO5

La función deberá mostrar por display el valor
que recibe. Reutilice las funciones creadas en
el punto 4 y 5.

*/
/*==================[inclusions]=============================================*/
#include "../inc/ejercicio_6.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
#define MASK_1 0b00000001
#define	MASK_2 0b00000010
#define MASK_3 0b00000100
#define MASK_4 0b00001000


void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t i;
	uint8_t resto;

	for( i = digits; i != 0; i--)
	{
		resto = data % 10;
		data = data/10;
		bcd_number[i-1] = resto;// me lo carga alrevez
	}

}//fin BinaryToBcd

typedef struct
{
	uint8_t pin;				//!< GPIO pin number
	uint8_t dir;				//!< GPIO direction ‘0’ IN;  ‘1’ OUT
}gpioConf_t;

void ControlPuertos(uint8_t bcd, gpioConf_t *puerto_ptr)
{
	if(bcd & MASK_1)
		{GPIOOn(puerto_ptr[0].pin);}
		else {GPIOOff(puerto_ptr[0].pin);}

	if(bcd & MASK_2)
		GPIOOn(puerto_ptr[1].pin);
		else GPIOOff(puerto_ptr[1].pin);

	if(bcd & MASK_3)
		GPIOOn(puerto_ptr[2].pin);
		else GPIOOff(puerto_ptr[2].pin);

	if(bcd & MASK_4)
		GPIOOn(puerto_ptr[3].pin);
		else GPIOOff(puerto_ptr[3].pin);

}

/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void funcion(uint32_t dato, uint8_t digitos, gpioConf_t *mapeo_pin_ptr, gpioConf_t *map_puertos)
{
	uint8_t vector[3];
	BinaryToBcd (dato, digitos, vector);

	ControlPuertos(vector[0], mapeo_pin_ptr);
	GPIOOn(map_puertos[0].pin);
	GPIOOff(map_puertos[0].pin);

	ControlPuertos(vector[1], mapeo_pin_ptr);
	GPIOOn(map_puertos[1].pin);
	GPIOOff(map_puertos[1].pin);

	ControlPuertos(vector[2], mapeo_pin_ptr);
	GPIOOn(map_puertos[2].pin);
	GPIOOff(map_puertos[2].pin);

}
/////////////////////////////////////
int main(void)
{


	uint32_t dato = 123; //valor aleatorio
	uint8_t digitos = 3;

	gpioConf_t mapeo_pin[4];

	mapeo_pin[0].pin = GPIO_LCD_1;
	mapeo_pin[1].pin = GPIO_LCD_2;
	mapeo_pin[2].pin = GPIO_LCD_3;
	mapeo_pin[3].pin = GPIO_LCD_4;

	mapeo_pin[0].dir = 1;
	mapeo_pin[1].dir = 1;
	mapeo_pin[2].dir = 1;
	mapeo_pin[3].dir = 1;

	gpioConf_t map_puertos[3];
	map_puertos[0].pin = GPIO_1;
	map_puertos[1].pin = GPIO_3;
	map_puertos[2].pin = GPIO_5;
	map_puertos[0].dir = 1;
	map_puertos[1].dir = 1;
	map_puertos[2].dir = 1;

	for(int i = 0; i<3; i++)
	{
		GPIOInit(map_puertos[i].pin, map_puertos[i].dir);
	}
	for(int i = 0; i<4; i++)
		{
			GPIOInit(mapeo_pin[i].pin, mapeo_pin[i].dir);
		}

while(1)
	{
	funcion(dato,digitos, mapeo_pin, map_puertos);

	}//fin while
	
	return 0;
}

/*==================[end of file]============================================*/

