/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*
 *  Escriba una función que reciba un dato de 32 bits,
 *  la cantidad de dígitos de salida y un puntero a un
 *  arreglo donde se almacene los n dígitos.
 *  La función deberá convertir el dato recibido a BCD,
 *  guardando cada uno de los dígitos de salida en
 *  el arreglo pasado como puntero.
 *  int8_t  BinaryToBcd (uint32_t data, uint8_t digits,
 *  uint8_t * bcd_number )
 *  {
 *
 *  }
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/ejercicio_4.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

uint8_t bcd_number[3];

int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t i;
	uint8_t resto;

	for( i = digits; i != 0; i--)
	{
		resto = data % 10;
		data = data/10;
		bcd_number[i-1] = resto;// me lo carga alrevez
	}

}//fin BinaryToBcd

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	
    while(1)
    {
    	BinaryToBcd(123,3, &bcd_number );

	}
    
	return 0;
}//main

/*==================[end of file]============================================*/

