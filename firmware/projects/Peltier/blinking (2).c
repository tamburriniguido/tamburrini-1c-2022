/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "delay.h"
#include "gpio.h"
#include "uart.h"
#include "pwm_sct.h"
#include "VNH3SP30.h"
#include "switch.h"
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/

uint32_t irBuffer[100]; //infrared LED sensor data
uint32_t redBuffer[100];  //red LED sensor data
int32_t bufferLength; //data length
int32_t spo2; //SPO2 value
int8_t validSPO2; //indicator to show if the SPO2 calculation is valid
int32_t heartRate; //heart rate value
int8_t validHeartRate; //indicator to show if the heart rate calculation is valid

rtc_t hora_config;
typedef enum  {hora, minutos, dia, mes, anio}tiempo_t;
/*==================[internal functions declaration]=========================*/
bool sentido = TRUE;
void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}
void Arriba(void){
	VNH3SP30_setSpeed(-100);
}

void Abajo(void){
	VNH3SP30_setSpeed(100);
}

void Apagar(void){
	VNH3SP30_brake(100);
}

bool var_1 = FALSE;
uint8_t temperature;
bool var_2 = FALSE;
uint8_t humidity;
bool var_3 = FALSE;
uint8_t duration;
bool set_clock = FALSE;

tiempo_t time_var = hora;

void Menu(void){
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);
	/******* RECUPERO VALORES NUMERICOS **********/
	if(var_1){
		temperature = dat;
		if((temperature<50) && (temperature>15)){
			UartSendString(SERIAL_PORT_PC, "Temperatura de consigna:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
			UartSendString(SERIAL_PORT_PC, "\r");
			dat = 0;
			var_1 = FALSE;
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Ingrese un valor entre 10 y 50 grados \r");
			dat = 0;
		}
	}
	if(var_2){
		humidity = dat;
		UartSendString(SERIAL_PORT_PC, "Humedad de consigna:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
		UartSendString(SERIAL_PORT_PC, "\r");
		dat = 0;
		var_2 = FALSE;
	}
	if(var_3){
		duration = dat;
		UartSendString(SERIAL_PORT_PC, "Duración de ensayo:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(dat, 10));
		UartSendString(SERIAL_PORT_PC, "\r");
		dat = 0;
		var_3 = FALSE;
	}
	if(set_clock){
		switch(time_var){
		case hora:
			hora_config.hour = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r");
			time_var = minutos;
			break;
		case minutos:
			hora_config.min = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el día:  \r");
			time_var = dia;
			break;
		case dia:
			hora_config.mday = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes:  \r");
			time_var = mes;
			break;
		case mes:
			hora_config.month = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el año:  \r");
			time_var = anio;
			break;
		case anio:
			hora_config.year = dat;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r");
			time_var = hora;
			dat = 0;
			set_clock = FALSE;
			break;
		}

	}

	/******* RECUPERO COMANDOS **********/
	switch(dat){
	case 't':
		UartSendString(SERIAL_PORT_PC, "Ingrese la variable #1:\r");
		var_1= TRUE;
		break;
	case 'h':
		UartSendString(SERIAL_PORT_PC, "Ingrese la variable #2:\r");
		var_2= TRUE;
		break;
	case 'T':
		UartSendString(SERIAL_PORT_PC, "Ingrese la variable #3:\r");
		var_3= TRUE;
		break;
	case 'C':
		UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual:\r");
		set_clock= TRUE;
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
/*	GPIOInit(GPIO_1, GPIO_INPUT);
	GPIOInit(GPIO_3, GPIO_INPUT);
	pwm_out_t pwm_pins[2] = {CTOUT0, CTOUT1};
	PWMInit(pwm_pins, 2, 20000);
*/
	pwm_out_t pwm_pins2[1] = {CTOUT0};
	VNH3SP30_begin(pwm_pins2, GPIO_1, GPIO_3, GPIO_5, CH1);
	SwitchActivInt(SWITCH_1, Arriba);
	SwitchActivInt(SWITCH_2, Abajo);
	SwitchActivInt(SWITCH_3, Apagar);
/*	GPIOActivInt(GPIOGP0,GPIO_1, Arriba, FALSE);
	GPIOActivInt(GPIOGP1,GPIO_3, Abajo, FALSE);
*/	serial_config UART_USB;
	UART_USB.baud_rate = 9600;
	UART_USB.port = SERIAL_PORT_PC;
	UART_USB.pSerial = Menu;
	UartInit(&UART_USB);

	uint8_t i;
	//PWMSetDutyCycle(CTOUT0, 0);
	//PWMSetDutyCycle(CTOUT1, 100);
	Apagar();
    while(1)
    {
    	if(sentido){
    	//	PWMSetDutyCycle(CTOUT0, 0);
    	//	PWMSetDutyCycle(CTOUT1,100);

    	}else{
    	//	PWMSetDutyCycle(CTOUT1, 0);
    	//	PWMSetDutyCycle(CTOUT0, 65);
    	}

	}
    
	return 0;
}

/*==================[end of file]============================================*/

