/*! @mainpage Template
 * *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 *
 *El Proyecto_Rocky_2 es una maquina de Punching Machine
 *la cual se encuentra apagada hasta que detecta a una persona
 *cerca, detectada la presona realiza una serie de juego de luces para llamar
 *la tencion del participante. Si la persona desea jugar, debera apretar la tecla
 *1 de la EDU, una vez apretada esta tecla el jugador tendra
 *un tiempo (en este caso 3 segundos) para golpear la bolsa. Cuando lo realiza se
 *prende una tira de luces que concuerdan con
 *la fuerza realizada por el jugador.Una vez terminado el golpe se realiza otro
 *la juego de luces acompañado con el valor del golpe por un display
 *LCD.
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	 GPIO3		|
 * | 	PIN2	 	| 	 GPIO5		|
 * | 	PIN3	 	| 	 GND		|
 *
 *
 * |     HC-SR4     |    EDU-CIAA    |
 * |:--------------:|:---------------|
 * |      ECHO      |     T_FIL2     |
 * |     TRIGGER    |     T_FIL3     |
 * |      +5V       |      +5V       |
 * |      GND       |      GND       |
 *
 *
 * |   lcditse0803  |    EDU-CIAA    |
 * |:--------------:|:--------------:|
 * |      LCD1      |      D1        |
 * |      LCD2      |      D2        |
 * |      LCD3      |      D3        |
 * |      LCD4      |      D4        |
 * |      GPIO1     |     SEL_0      |
 * |      GPIO3     |     SEL_1      |
 * |      GPIO5     |     SEL_2      |
 * |      +5V       |      +5V       |
 * |      GND       |      GND       |
 *
 *
 * |  XFPM_050KPGR  |    EDU-CIAA    |
 * |:--------------:|:--------------:|
 * |     Vout       |      CH1       |
 * |     GND        |      GND       |
 * |     +5V        |      +5V       |
 *
 *
 * |   RGB_gauge    |    EDU-CIAA    |
 * |:--------------:|:--------------:|
 * |     GND        |      GND       |
 * |     +5V        |      +5V       |
 * |   Data Input   |     GPIO8      |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                  |
 * |:----------:|:-----------------------------|
 * | 01/01/2022 | Document creation		       |
 * | 02/01/2022	| A functionality is added     |
 * | 10/05/2022	| Realizacion Diagrama flujo   |
 * | 17/05/2022 | Realizacion Diagrama flujo   |
 * | 24/05/2022 | Inicio de programacion       |
 * | 31/05/2022 | Documento Terminado          |
 * @author Guido Tamburrini
 *
 */
/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Rocky_2.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "analog_io.h"

#include "uart.h"
#include "hc_sr04.h"
#include "lcditse0803.h"
#include "XFPM_050KPGR.h"//lo cree yo
#include "RGB_gauge.h"//rueda de luces

/*==================[macros and definitions]=================================*/
// VARIABLES GLOBALES
#define contador_max 300
#define GPIO8 8
#define NUM_PIXEL 12
uint32_t maximo;
bool prender = false;
uint32_t contador = 0;
uint8_t distancia;
uint8_t presencia = false;

uint32_t vector_fuerza[contador_max];
/*el tamaño del vector_fuerza[*] esta
 *relacionado al numero de veces que
 *muestrea el sensor de presion*/
/* ====== */
void juego_luces(void)
{
	LedOn(LED_1);
	LedOn(LED_2);
	LedOn(LED_3);
	DelaySec(1);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
	DelaySec(1);
}//juego_luces()
/* === Apretar+ === */
void juego_luces_ganador(void)
{
	LedOn(LED_1);
	DelaySec(1);
	LedOff(LED_1);
	DelaySec(1);
	LedOn(LED_1);
	DelaySec(1);
	LedOff(LED_1);
}//end juego_luces_ganador()
/* === Apretar+ === */
/*========================================*/
void TeclaUno(void)//Bandera
{
	prender = true;
	LedsOffAll();
}
/* === Apretar+ === */
void F_timer(void)//Funcion del Timer
{
	vector_fuerza[contador]=Leo_Presion();
	contador++;

	Gauge_line(Leo_Presion());//RUEDA DE LUCES

	//Manda los valores SERIAL-PORT
	UartSendString(SERIAL_PORT_PC, UartItoa((int)Leo_Presion(), 10));//10 es la base
	UartSendString(SERIAL_PORT_PC, "\r");// sacado del .h
}
/* === Apretar+ === */
void fuerza_maxima(void)
{
	for(uint32_t i=0; i<contador_max; i++)
	{
		if(maximo < vector_fuerza[i])
		{ maximo = vector_fuerza[i]; }
	}//for
	LcdItsE0803Write(maximo);
	juego_luces_ganador();

}//fuerza_maxima()
/* === Apretar+ === */
int main(void)
{
	//Inicializacion
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LcdItsE0803Init();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);//ultrasonido
	
	SensorInit(CH1);//uint8_t entrada_sensor presion que yo cree
	InitRGB_Gauge(12,8,120,400);//rueda de luz

	//UART (serial - port)
	serial_config dato_uart;//tipo de dato del drive uart.h
	dato_uart.port = SERIAL_PORT_PC; //puerto sacado del .h
	dato_uart.baud_rate = 115200 ; //valor de velocidad,
								   //debe ser el mismo que
								   //la velocidad del terminal Hterm
	UartInit(&dato_uart);//paso la estructura

	SwitchActivInt(SWITCH_1, &TeclaUno);//cambio de bandera

	// Timer (INTERRUPCION)
	timer_config dato_timer;//tipo de dato del drive timer.h
	dato_timer.timer= 1;//lleno el estruct
	dato_timer.period = 100;//lleno el estruct
	dato_timer.pFunc = F_timer;//Funcion a Ejecutar en la interrupcion
	TimerInit(&dato_timer);


	while(1)
	{
		do
		{
			distancia = HcSr04ReadDistanceInCentimeters();
			if (distancia < 30)
			{presencia = true;}
		}while(presencia != true);
		LcdItsE0803Write(000);
		while(!prender){juego_luces();}

		TimerStart(dato_timer.timer);
		do{/*nada*/}while(contador < 30);
		fuerza_maxima();

		TimerStop(dato_timer.timer);
		TimerReset(dato_timer.timer);

		prender = false;
		contador = 0;
		Gauge_line(120);//es xq el sensor de presion por como estaba armado el set up tiene una presion inicial de ese valor
		presencia = false;

	}//while
	return 0;
}//main
/* === Apretar+ === */

/*==================[end of file]============================================*/

