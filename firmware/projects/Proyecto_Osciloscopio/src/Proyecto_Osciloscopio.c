/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*
 * Actividad 4 - Proyecto: Osciloscopio
 *
Diseñar e implementar una aplicación,
basada en el driver analog_io.h y el driver
de transmisión serie uart.h, que digitalice
una señal analógica y la transmita a un
graficador de puerto serie de la PC.
Se debe tomar la entrada CH1 del conversor AD
y la transmisión se debe realizar por la UART
conectada al puerto serie de la PC, en un
formato compatible con un graficador por puerto
serie.
Sugerencias:
Disparar la conversión AD a través de una
interrupción periódica de timer.
Utilice una frecuencia de muestreo de 500Hz.
Obtener los datos en una variable que le
permita almacenar todos los bits del conversor.
Transmitir los datos por la UART en formato
ASCII a una velocidad de transmisión suficiente
para realizar conversiones a la frecuencia
requerida.
Utilizar el graficador serie de código
abierto que se encuentra en esta página:
https://x-io.co.uk/serial-oscilloscope/.
Se deben enviar los datos convertidos separados
por el carácter de fin de línea ("11,22,33\r"
para el caso de 3 canales con los datos 11, 22
y 33 para el canal 1, 2 y 3 respectivamente).
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Osciloscopio.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
/*==================[macros and definitions]=================================*/
#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[internal data definition]===============================*/
uint16_t i;
uint16_t dato_convertido;
/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

//------------------//
void inicio_conversion()
{ AnalogStartConvertion(); }
//------------------//
void leo_datos()
{
	AnalogInputRead(CH1, &dato_convertido);

	//mandar los valores SERIAL-PORT
	UartSendString(SERIAL_PORT_PC, UartItoa(dato_convertido, 10));//10 es la base
	UartSendString(SERIAL_PORT_PC, " \r");// sacado del .h

}
void funcion()
{
	AnalogOutputWrite(ecg[i]);
	i++;
	if (i == BUFFER_SIZE)
		i = 0;
}

//------------------//
int main(void){
	SystemClockInit();
	AnalogOutputInit();

	//UART (serial - port)
	serial_config dato_uart;//tipo de dato del drive uart.h
	dato_uart.port = SERIAL_PORT_PC; //puerto sacado del .h
	dato_uart.baud_rate = 115200 ; //valor de velocidad,
								 //debe ser el mismo que
								 //la velocidad del terminal Hterm
	UartInit(&dato_uart);//paso la estructura
	
	//TIMER 1
	timer_config dato_timer;
	dato_timer.timer = TIMER_A;
	dato_timer.period = 2;//dato de la consigna
	dato_timer.pFunc = inicio_conversion;
	TimerInit(&dato_timer);
	TimerStart(dato_timer.timer);

	//TIMER 2
	timer_config dato_timer_2;
	dato_timer_2.timer = TIMER_B;
	dato_timer_2.period = 5;
	dato_timer_2.pFunc = funcion;
	TimerInit(&dato_timer_2);
	TimerStart(dato_timer_2.timer);

	//ANALOG_IO
	analog_input_config dato_analog;
	dato_analog.input = CH1;
	dato_analog.mode = AINPUTS_SINGLE_READ ;
	dato_analog.pAnalogInput = &leo_datos ;//funcio que queiro q se ejecute al hacer la interrupcion
	AnalogInputInit(&dato_analog);
	
	while(1)
    {}
    
	return 0;
}//end main

/*==================[end of file]============================================*/

