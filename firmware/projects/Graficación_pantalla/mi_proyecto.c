/* Copyright 2018, Eduardo Filomena - Gonzalo Cuenca
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */	

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "mi_proyecto.h"       /* <= own header */
#include "ili9341.h"
#include "fonts.h"
#include "gpio.h"
#include "spi.h"
#include "fonts.h"
#include "realtimeplot.h"
#include "delay.h"

const char ecg_data[231]={
76, 83, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
 99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
 74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

int main(void)
{ 
   /* perform the needed initialization here */
	SystemCoreClockUpdate();
	Board_Init();
	ILI9341Init(SPI_1, GPIO_5, GPIO_1, GPIO_3);
	ILI9341Rotate(ILI9341_Landscape_1);
	ILI9341Fill(ILI9341_DARKGREY);
	//Variables
	plot_t plot1 = {
			10,
			10,
			145,
			105,
			75,
			ILI9341_BLACK
	};
	RTPlotInit(&plot1);

	plot_t plot2 = {
			165,
			10,
			145,
			105,
			30,
			ILI9341_BLACK
	};
	RTPlotInit(&plot2);

	plot_t plot3 = {
			10,
			125,
			145,
			105,
			50,
			ILI9341_BLACK
	};
	RTPlotInit(&plot3);

	plot_t plot4 = {
			165,
			125,
			145,
			105,
			50,
			ILI9341_BLACK
	};
	RTPlotInit(&plot4);

	signal_t ecg = {
			40,
			0,
			ILI9341_BLUE,
			0,
			0
	};
	RTSignalInit(&plot1, &ecg);

	signal_t ecg2 = {
			40,
			0,
			ILI9341_ORANGE,
			0,
			0
	};
	RTSignalInit(&plot2, &ecg2);

	signal_t ramp = {
			30,
			0,
			ILI9341_RED,
			0,
			0
	};
	RTSignalInit(&plot3, &ramp);

	signal_t ramp_neg = {
			10,
			20,
			ILI9341_GREEN,
			0,
			0
	};
	RTSignalInit(&plot4, &ramp_neg);

	uint16_t i = 0;

	while(1){
		if (i > 230){
			i = 0;
		}
		RTPlotDraw(&ecg, ecg_data[i]);
		RTPlotDraw(&ecg2, ecg_data[i]);
		RTPlotDraw(&ramp, i);
		RTPlotDraw(&ramp_neg, 230-i);
		i++;
		DelayMs(5);
	}

	return 0;


}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

