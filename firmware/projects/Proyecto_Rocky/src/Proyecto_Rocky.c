/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto_Rocky.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "hc_sr04.h"
#include "led.h"
#include "delay.h"
#include "gpio.h"
#include "lcditse0803.h"
#include "stdint.h"
#include "timer.h"
#include "analog_io.h"
#include "buzzer.h"
/*==================[macros and definitions]=================================*/
uint8_t prender = 0;
int16_t fuerza_max = 0;
uint16_t dato_convertido;




/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

//Interrupcion
void TeclaUno()
{ prender =! prender; }
/*==================*/

void Juego_luces(void)
{}//end Juego_luces
/*--------------------*/
void Juego_luces_Perdedor(void)
{}//end Juego_luces_Perdedor
/*--------------------*/
void Juego_luces_Ganador(void)
{}//end Juego_luces_Ganador
/*--------------------*/
void Sonidos(void)
{}//end Sonidos
/*--------------------*/
void Llamador_de_Atencion (int16_t valor_distancia)
{
	if (valor_distancia < 50)
	{
		for (uint8_t i=0; i<3; i++)
		{
			Juego_luces();
			Sonidos();
		}
	}
}
/*==================*/
//////// y esto????
void inicio_conversion()
{ AnalogStartConvertion(); }

////////
/*--------------------*/
void leo_datos()
{
	uint8_t presion;
	presion = Leo_Presion();
	LcdItsE0803Write(presion);


	//tira de led
	//vector(i)

}
/*--------------------*/
void fuerza_maxima(int16_t fuerza)
{
	if (fuerza_max < fuerza)
	{
		fuerza_max = fuerza;
		LcdItsE0803Write(fuerza_max);
		//Juego_luces(); para el ganador
		//Sonidos(void); para el genador
	}
	else
	{
		LcdItsE0803Write(fuerza);
		//Juego_luces(); para el perdedor
		//Sonidos(void); para el perdedor
		LcdItsE0803Write(0);//seria para borrar los valores
	}

}//end fuerza_maxima
/*--------------------*/
void Funcion_A (void)
{
	//pego
	//leo_datos()
	//if(tiempo = 30)
	//{llamador = true}
	//tiempo++

}

/*--------------------*/
int main(void)
{
	//Inicializacion
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LcdItsE0803Init();

	WS2812bInit(GPIO_0);// tira de led GPIO0 - GPIO8

	AnalogOutputInit();
	BuzzerInit();
	BuzzerOn();

	//Configuraciones de los puertos
	HcSr04Init(GPIO_T_FIL2 , GPIO_T_FIL3);//ultrasonido

	//Interrupcion
	SwitchActivInt(SWITCH_1, &TeclaUno);

	//Cosas de Timer
	timer_config dato_timer;//tipo de dato del drive timer.h
	//lleno el estruct
	dato_timer.timer= 1;
	dato_timer.period = 100;
	dato_timer.pFunc = Funcion_A;//fijate si es esa funcion
	TimerInit(&dato_timer);
	TimerStart(dato_timer.timer);

	//Sensor de presion XFPM_050KPGR
	analog_input_config dato_sensor;
	dato_sensor.input = CH2;
	dato_sensor.mode = AINPUTS_SINGLE_READ;
	dato_sensor.pAnalogInput = &leo_datos;//funcio que queiro q se ejecute al hacer la interrupcion
	AnalogInputInit(&dato_sensor);

    while(1)
    {


	}


	return 0;
}// end main

/*==================[end of file]============================================*/

