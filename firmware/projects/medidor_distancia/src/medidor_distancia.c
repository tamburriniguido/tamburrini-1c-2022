/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*
1)
Conectar a la EDU-CIAA un sensor de
ultrasonido HC-SR04 y una pantalla LCD
y utilizando los drivers provistos por
la cátedra implementar una aplicación que
permita:

Mostrar distancia medida utilizando los
leds de la siguiente manera:

Si la distancia está entre 0 y 10 cm,
encender el LED_RGB_B (Azul).
Si la distancia está entre 10 y 20 cm,
encender el LED_RGB_B (Azul) y LED_1.
Si la distancia está entre 20 y 30 cm,
encender el LED_RGB_B (Azul), LED_1 y LED_2
Si la distancia es mayor a 30 cm, encender el
LED_RGB_B (Azul), LED_1, LED_2 y LED_3.

Mostrar el valor de distancia en cm utilizando
el display LCD.
Usar TEC1 para activar y detener la medición.
Usar TEC2 para mantener el resultado (“HOLD”).
Refresco de medición: 1 s

*/


/*==================[inclusions]=============================================*/
#include "../inc/medidior_distancia.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr04.h"
#include "delay.h"
#include "gpio.h"
#include "lcditse0803.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void Led_de_Distancia (int16_t valor_distancia)
{
	if (valor_distancia < 10)
	{
		LedOn(LED_RGB_B);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if ((11 < valor_distancia) && (valor_distancia < 20))
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_2);
		LedOff(LED_3);
	}
	if ((21 < valor_distancia) && (valor_distancia < 30))
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_3);
	}
	if(valor_distancia > 30)
	{
		LedOn(LED_RGB_B);
		LedOn(LED_1);
		LedOn(LED_2);
		LedOn(LED_3);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
	}

}//end LedDistancia

int main(void){
	//Inicializacion
	SystemClockInit();	//inicial el clock de la CIAA
	LedsInit();
	SwitchesInit();
	LcdItsE0803Init();

	//Declaro las funciones
	HcSr04Init(GPIO_T_FIL2 , GPIO_T_FIL3);

	//Declaro variables
	uint8_t teclas;
	uint16_t distancia = 0;
	uint8_t hold = 0;
	uint8_t prender = 0 ;
	
	while(1){
			teclas = SwitchesRead();
			switch(teclas){
				case SWITCH_1:
					prender =! prender;
					break;
				case SWITCH_2:
					hold =! hold;
					break;
			}//end switch
			if(prender)
			{
				distancia = HcSr04ReadDistanceInCentimeters();
				if (!hold)
				{
					Led_de_Distancia(distancia);
					LcdItsE0803Write(distancia);
				}
			}//fin primer if
			else
			{
				LedOOff(LED_RGB_R);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				LcdItsE0803Write(0);
			}//fin else
			DelaySec(1);
	}//fin while
	return 0;
}//end main

/*==================[end of file]============================================*/

