/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */

/*
 * ################ Consigna ################
 * Modifique la aplicación 1_blinking_switch
 * de manera de hacer titilar los leds verde,
 * amarillo y rojo al mantener presionada las
 * teclas 2, 3 y 4 correspondientemente.
 * También se debe poder hacer titilar los leds
 * individuales del led RGB, para ello se deberá
 * mantener presionada la tecla 1 junto con la
 * tecla 2, 3 o 4 correspondientemente.
 * En el siguiente video se puede observar el
 * funcionamiento deseado del sistema:

*/

/*==================[inclusions]=============================================*/
#include "../inc/1_blinking_switch_MODIFICADO.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void Delay(uint32_t tiempo)
{
	uint32_t i;
	for(i=tiempo; i!=0; i--)
	{asm  ("nop");}
}//end Delay

int main(void){
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	uint32_t tiempo = 3000000;
	uint8_t teclas;

	LedOn(LED_3);
	Delay (tiempo);
	LedOff(LED_3);

    while(1){
    	teclas  = SwitchesRead();
    	switch(teclas){
			case SWITCH_1 |SWITCH_2:
				LedOn(LED_RGB_B);
				Delay(1);
				LedOff(LED_RGB_B);
				Delay(1);
				break;

			case SWITCH_1 |SWITCH_3:
				LedOn(LED_RGB_R);
				Delay(1);
				LedOff(LED_RGB_R);
				Delay(1);
				break;

			case SWITCH_1 |SWITCH_4:
				LedOn(LED_RGB_G);
				Delay(1);
				LedOff(LED_RGB_G);
				Delay(1);
				break;

			case SWITCH_2:
				LedOn(LED_1);
				Delay(1);
				LedOff(LED_1);
				break;

			case SWITCH_3:
				LedOn(LED_2);
				Delay(1);
				LedOff(LED_2);
				break;

			case SWITCH_4:
				LedOn(LED_3);
				Delay(1);
				LedOff(LED_3);
				break;
    	}//end switch

	}//end while

	return 0;
}//end main

/*==================[end of file]============================================*/

