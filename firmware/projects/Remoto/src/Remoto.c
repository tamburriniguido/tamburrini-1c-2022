/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Name
 *
 */
/*=========== Consigna =====================*/
 /*Para corroborar la señal de la placa
 Arduino  desarrolle un firmware que prenda
 el LED_2 de la EDU-CIAA a partir de la lectura
 continua del pin TCOL_1.  El LED 2 debe
 encenderse o apagarse una vez se detecte el
 flanco ascendente de la señal de reloj
 mencionada como se observa en la Figura 2.
 Utilice los drivers de gpio.h y led.h
 provistos por la cátedra.

¿Cual es la frecuencia resultante de
encendido/apagado del LED?
*/
/*==================[inclusions]=============================================*/
#include "../inc/Remoto.h"       /* <= own header */
#include "gpio.h"
#include "led.h"
#include "delay.h"
#include "bool.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/
uint32_t frecuencia = 0;

bool pulso_anterior = 0;
bool pulso_posterior = 0;
bool estado = 0;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
void SysInit(void)
{
	LedsInit();
	GPIOInit(GPIO_T_COL1, GPIO_INPUT);
}// end SysInit

//---------------//
void encendido(void)
{
		LedOn(LED_1);
		DelaySec(1);
		LedOff(LED_1);
		DelaySec(1);
		LedOn(LED_1);
		DelaySec(1);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);

}//end encendido

//-------------//
void led (estado)
{
	if(estado == 1)
	{
		LedOff(LED_RGB_B);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_1);
		LedOn(LED_2);
		LedOff(LED_3);
		frecuencia++;
	}
	else
	{
		LedOff(LED_RGB_B);
		LedOff(LED_RGB_R);
		LedOff(LED_RGB_G);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
	}
}//end led

//---------------//
void flanco(void)
{
	pulso_anterior = GPIORead(GPIO_T_COL1);
	DelayMs(1);
	pulso_posterior = GPIORead(GPIO_T_COL1);

	if((pulso_anterior == 0) && (pulso_posterior == 1))
	{
		estado = !estado;
		led(estado);
	}

}//end flanco
//---------------//

int main(void){
	SystemClockInit();
	SysInit();

	encendido();
    while(1)
    {
    	flanco();
	}
    
	return 0;
}//end main

/*==================[end of file]============================================*/

