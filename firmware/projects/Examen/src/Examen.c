/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 * El presente el examen de la materia de Electronica Programable
 * el cual consiste en medir la temperatura con su sensor de ultrasonido
 * mostrar el valor de tempratura por una pantalla led y a su vez
 * mostrar los valores por puerto serie
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *

 * |     HC-SR4     |    EDU-CIAA    |
 * |:--------------:|:---------------|
 * |      ECHO      |     T_FIL2     |
 * |     TRIGGER    |     T_FIL3     |
 * |      +5V       |      +5V       |
 * |      GND       |      GND       |
 *
 *
 * |   lcditse0803  |    EDU-CIAA    |
 * |:--------------:|:--------------:|
 * |      LCD1      |      D1        |
 * |      LCD2      |      D2        |
 * |      LCD3      |      D3        |
 * |      LCD4      |      D4        |
 * |      GPIO1     |     SEL_0      |
 * |      GPIO3     |     SEL_1      |
 * |      GPIO5     |     SEL_2      |
 * |      +5V       |      +5V       |
 * |      GND       |      GND       |
 *
 * |   Termopila    |    EDU-CIAA    |
 * |:--------------:|:--------------:|
 * |     +3.3V      |     +3.3V      |
 * |      GND       |      GND       |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Guido Tamburrini
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Examen.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "led.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"
#include "hc_sr04.h"
#include "lcditse0803.h"
#include "Termopila.h"
/*==================[macros and definitions]=================================*/
uint8_t distancia;
uint8_t presencia = false;
float temp_promedio;
float vector_temperatura[10];
/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/
void temperatura_promedio(void)
{
	float temperatura = 0;
	for(uint32_t i=0; i<10; i++)
		{
			temperatura = vector_temperatura[i];
			temp_promedio = temp_promedio + temperatura;
		}//for
	temp_promedio = temp_promedio/10;
}
/* === Apretar+ === */
void F_timer(void)//Funcion del Timer
{
	vector_temperatura[contador]=Leo_Temperatura();
	contador++;
}
/* === Apretar+ === */

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	LcdItsE0803Init();
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);//ultrasonido

	//UART (serial - port)
	serial_config dato_uart;//tipo de dato del drive uart.h
	dato_uart.port = SERIAL_PORT_PC; //puerto sacado del .h
	dato_uart.baud_rate = 115200 ; //valor de velocidad,
									   //debe ser el mismo que
									   //la velocidad del terminal Hterm
	UartInit(&dato_uart);//paso la estructura
	
	// Timer (INTERRUPCION)
	timer_config dato_timer;//tipo de dato del drive timer.h
	dato_timer.timer= 1;//lleno el estruct
	dato_timer.period = 100;//lleno el estruct
	dato_timer.pFunc = F_timer;//Funcion a Ejecutar en la interrupcion
	TimerInit(&dato_timer);
	
    while(1)
    {
    	do
    	{
    		distancia = HcSr04ReadDistanceInCentimeters();
    		if(12 < distancia)
    		{
    			LedOn(LED_1);
    			DelaySec(1);
    			LedOff(LED_1);
    		}
    		if(distancia < 8)
    		{
    			LedOn(LED_3);
    			DelaySec(1);
    			LedOff(LED_3);
    		}
    		if (8< distancia <12)//tendria q ser menor igual
    		{
    			LedOn(LED_2);
    			DelaySec(1);
    			LedOff(LED_2);
    			presencia = true;
    		}
    	}while(presencia != true);

    	TimerStart(dato_timer.timer);
    	do{/*nada*/}while(contador < 10);

    	temperatura_promedio();
    	LcdItsE0803Write(temp_promedio);
    	if (temp_promedio < 37.5)
    	{
    		LedOn(LED_RGB_G);
    		DelaySec(1);
    		LedOff(LED_RGB_G);
    	}
    	if (37.5 <temp_promedio)
    	{
    	   	LedOn(LED_RGB_R);
    	    DelaySec(1);
    	    LedOff(LED_RGB_R);
    	}

    	//Manda los valores SERIAL-PORT
    	UartSendString(SERIAL_PORT_PC, UartItoa("Temperatura: "temp_promedio" °C Persona a "distancia, 10));//10 es la base
    	UartSendString(SERIAL_PORT_PC, "\r");// sacado del .h

    	TimerStop(dato_timer.timer);
    	TimerReset(dato_timer.timer);

    	prender = false;
    	contador = 0;
    	temp_promedio = 0;
    }//while

	return 0;
}//end main

/*==================[end of file]============================================*/

